import pandas as pd
pd.set_option('display.max_rows', 1000)
pd.set_option('display.max_columns', 1000)


data = pd.read_csv('data.csv')

data['col_name'].hist(bins=7)
        
data.plot(subplots=True, figsize=(20, 15))

data.hist(bins=8, figsize=(20,15));

data[ data['BILL_AMT1']<=-20000 ]

data.columns

from urllib.request import urlopen
def urlimport(x):
    exec( urlopen(x).read(), globals())

urlimport('https://bitbucket.org/gwr1/helpers/raw/43ed2c27f3324c3c8518967be8432ff261840582/plot_sorted.py')

def count_uniq(col):
    return len(col.unique())

data_uniq_cnt = data.apply(count_uniq, axis=0)
data_uniq_cnt

import seaborn as sns
sns.set(style="ticks")

sns.pairplot(data, hue="PAY_0")

