from matplotlib import pyplot as plt
import numpy as np
import pandas as pd


def plot_sorted(data, cols=6, how='plot', except_cols=[], compare=False, figsize=(16, 10), **kwargs):
    rows = np.ceil( data.shape[1]/cols ) if not compare else np.ceil( data[0].shape[1]/cols )
    
    fig = plt.figure(figsize=figsize)
    columns_all = data if not compare else data[0]
    axarr = []
    _columns = columns_all.columns.difference( except_cols )
    
    for i, col in enumerate(_columns):
        if i // 6 > 0:
            sharex = axarr[i % 6]
            plt.setp(axarr[i % 6].get_xticklabels(), visible=False)
        else:
            sharex = None
        if i % 6 > 0:
            sharey = axarr[i // 6]
        else:
            sharey = None
        ax = fig.add_subplot(rows, cols, i + 1)
        axarr.append(ax)
        if i % 6 > 0:
            plt.setp(ax.get_yticklabels(), visible=True)
            
        plot_data = columns_all[col].sort_values().reset_index()[col]
        
        labels = []
        if compare:
            plot_data = []
            for compare_num, sub_data in enumerate( data ):
                labels.append( col + str(compare_num ) )
                plot_data.append( sub_data[col].sort_values().reset_index()[col] )
                
        getattr( ax, how)( plot_data, **kwargs )
        ax.set_title(col)
        if len(labels):
            ax.legend( labels )
        ax.grid(True)
    fig.subplots_adjust(wspace=0.3, hspace=0.3)
    plt.show();